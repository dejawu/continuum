const get = async path => {
	return await fetch(path, {
		method: "GET",
		headers: {
			"Content-Type": "application/json",
		},
		credentials: "include",
	}).then(async r => [await r.json(), r.ok]);
};

const post = async (path, body = {}) => {
	return await fetch(path, {
		body: JSON.stringify(body),
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		credentials: "include",
	}).then(async r => [await r.json(), r.ok]);
};

const call = async task => {
	try {
		const result = await task;
		return [result, null];
	} catch (err) {
		return [null, err];
	}
};

export { get, post, call };
