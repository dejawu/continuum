import React, { Component } from "react";
import { request } from "graphql-request";
import Button from "Common/js/components/Button";
import { Creatable as CreatableSelect } from "react-select";
import MESSAGES from "Common/js/Messages";
import { call } from "Common/js/call";

const ENDPOINT = require("../../../secrets.json")["domain"] + "/graphql";

const WORKSPACES_QUERY = `
	query {
		viewer {
			workspaces {
				id
				name
			}
		}
	}
`;

const ADD_WORKSPACE_MUTATION = `
	mutation AddWorkspace($name: String!) {
		addWorkspace(name: $name) {
			id
		}
	}
`;

class WorkspaceSelector extends Component {
	constructor(props) {
		super(props);

		this.state = {
			workspaces: [
				{
					value: "null", // react-select expects a string for ID.
					label: "(None)",
				},
			],
			selectedWorkspace: {
				id: null,
				name: "(None)",
			},
			ready: false,
			error: false,
		};

		chrome.storage.local.get(["workspace"], ({ workspace }) => {
			console.log(workspace);
			if (workspace) {
				this.setState({
					selectedWorkspace: workspace,
				});
			}
		});

		chrome.runtime.onMessage.addListener(({ message, ...payload }) => {
			if (message === MESSAGES.SELECT_WORKSPACE) {
				this.setState({
					selectedWorkspace: payload,
				});
			}
		});

		(async () => {
			const [result, err] = await call(request(ENDPOINT, WORKSPACES_QUERY));
			if (err) {
				this.setState({ error: true });
				return;
			}

			console.log(result.viewer.workspaces);

			this.setState({
				workspaces: [
					{
						value: "null",
						label: "(None)",
					},
					...result.viewer.workspaces.map(({ id, name }) => ({
						value: id,
						label: name,
					})),
				],
				ready: true,
			});
		})();
	}

	async addWorkspace(name) {
		const [result, err] = await call(
			request(ENDPOINT, ADD_WORKSPACE_MUTATION, {
				name: name,
			}),
		);

		if (err) {
			this.setState({
				error: true,
			});
			return;
		}

		const {
			addWorkspace: { id },
		} = result;

		this.setState((prevState, props) => ({
			workspaces: [
				...prevState.workspaces,
				{
					value: id,
					label: name,
				},
			],
		}));

		this.selectWorkspace({
			id,
			name,
		});
	}

	selectWorkspace(workspace) {
		this.setState({
			selectedWorkspace: workspace,
		});

		if (typeof this.props.onSelectWorkspace === "function") {
			this.props.onSelectWorkspace(workspace);
		}
	}

	render() {
		if (!this.state.ready) {
			return (
				<div className="workspaceSelector">
					<p>Loading...</p>
				</div>
			);
		}

		if (this.state.error) {
			return (
				<div className="workspaceSelector">
					<p>An error occurred.</p>
				</div>
			);
		}

		return (
			<div className="workspaceSelector">
				<CreatableSelect
					options={this.state.workspaces}
					value={{
						value: this.state.selectedWorkspace.id,
						label: this.state.selectedWorkspace.name,
					}}
					formatCreateLabel={name => `Create workspace: ${name}`}
					placeholder={"Select workspace"}
					onChange={({ value, label }, { action }) => {
						if (action !== "select-option") {
							return;
						}

						if (value === "null") {
							value = null;
						}

						this.selectWorkspace({
							id: value,
							name: label,
						});
					}}
					onCreateOption={name => {
						this.addWorkspace(name);
					}}
					isDisabled={this.state.loading}
					isLoading={this.state.loading}
				/>
			</div>
		);
	}
}

export default WorkspaceSelector;
