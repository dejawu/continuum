import React, { Component } from "react";

const Button = props => {
	return (
		<a
			className={`button-${props.color}`}
			href="#"
			onClick={e => {
				e.preventDefault();
				props.onClick();
			}}
		>
			{props.children}
		</a>
	);
};

export default Button;
