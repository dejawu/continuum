const path = require("path");

const COMMON = path.resolve(__dirname, "common");

const CHROME_SRC = path.resolve(__dirname, "chrome/src");
const CHROME_BUILD = path.resolve(__dirname, 'chrome/dist/js');

const CHROME = {
	mode: "development",
	entry: {
		popup: path.resolve(CHROME_SRC, "js/popup.js"),
		background: path.resolve(CHROME_SRC, "js/background.js"),
	},
	output: {
		path: CHROME_BUILD,
		filename: "[name].js"
	},
	devtool: "source-map",
	node: false,
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: [/node_modules/, path.resolve(CHROME_SRC, "scss")],
				use: {
					loader: "babel-loader",
				}
			},
			{
				test: /\.scss$/,
				use: [
					"style-loader",
					"css-loader",
					"sass-loader"
				]
			},
		]
	},
	resolve: {
		alias: {
			Common: COMMON,
		}
	}
};

const WEB_SRC = path.resolve(__dirname, 'web/client');
const WEB_BUILD = path.resolve(__dirname, 'web/static');

const WEB = {
	mode: "development",
	entry: path.resolve(WEB_SRC, 'jsx/index.jsx'),
	output: {
		path: path.resolve(WEB_BUILD),
		filename: 'bundle.js',
	},
	node: false,
	module: {
		rules: [
			{
				test: /\.jsx?/,
				include: path.resolve(WEB_SRC, 'jsx/'),
				loader: 'babel-loader',
			},
			{
				test: /\.scss$/,
				use: [
					"style-loader",
					"css-loader",
					"sass-loader",
				]
			}
		]
	},
	resolve: {
		alias: {
			Common: COMMON,
		}
	}
};

module.exports = [CHROME, WEB];