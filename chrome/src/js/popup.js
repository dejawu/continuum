import "../scss/popup.scss";
import React from "react";
import { render } from "react-dom";
import Popup from "./Popup.jsx";

render(<Popup />, document.getElementById("root"));
