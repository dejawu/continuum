import Ext from "./Ext";
import MESSAGES from "Common/js/Messages";

const ext = new Ext();

const DOMAIN = require("../../../secrets.json")["domain"];

//idMap[chrome tab id] = continuum branch id
const idMap = [];

// flag used to temporarily disable tab creation listeners while workspace loading is in progress
let restoring = false;

chrome.tabs.onCreated.addListener(async tab => {
	if (restoring) {
		return;
	}

	if (tab.url === "chrome://history/") {
		return;
	}

	if (tab.url === "chrome://newtab/" && !idMap.includes(tab.id)) {
		idMap[tab.id] = await ext.createNewBranch(tab);
	} else if (tab.url.startsWith("chrome://") || tab.url.startsWith(DOMAIN)) {
		return;
	} else {
		if (!idMap[tab.openerTabId]) {
			console.log("Ignoring new child on untracked branch: ");
			console.log(tab.url, tab.title, tab.openerTabId);
			return;
		}
		idMap[tab.id] = await ext.createChildBranch(idMap[tab.openerTabId], tab);
	}
});

chrome.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => {
	if (changeInfo.status === "complete") {
		console.log("Pushing update:");
		console.log({
			favIconUrl: tab.favIconUrl,
			url: tab.url,
			title: tab.title,
		});

		if (tab.url !== "chrome://newtab/") {
			await ext.addStep(idMap[tab.id], tab);
		}
	}
});

// background serves as central dispatch for the extension.
// content and popup scripts initiate actions by sending messages here.
chrome.runtime.onMessage.addListener(async ({ message, ...payload }) => {
	switch (message) {
		case MESSAGES.SELECT_WORKSPACE:
			const tabs = await ext.selectWorkspace(payload);

			restoring = true;
			idMap.splice(0, idMap.length); // empty idMap without reassignment

			await Promise.all(
				tabs
					.sort(({ timestamp: a, timestamp: b }) => b - a)
					.reverse()
					.map(
						({ branchId, url }) =>
							new Promise(resolve => {
								chrome.tabs.create(
									{
										url,
										index: 0,
									},
									({ id: newTabId }) => {
										idMap[newTabId] = branchId;
										console.log(idMap);
										resolve();
									},
								);
							}),
					),
			);

			// close tabs not found in ID map
			const openTabs = await new Promise(resolve => {
				chrome.tabs.query({}, tabs => resolve(tabs));
			});

			openTabs.forEach(({ id }) => {
				if (!idMap[id]) {
					chrome.tabs.remove(id);
				}
			});

			// load continuum page if workspace has no tabs
			if (tabs.length === 0) {
				chrome.windows.create({
					url: DOMAIN,
					top: 0,
					left: 0,
				});
			}

			restoring = false;
			break;
		default:
			throw new Error("Invalid message: " + message);
	}
});
