import React, { Component } from "react";

import { get } from "Common/js/call";
import WorkspaceSelector from "Common/js/components/WorkspaceSelector";

import MESSAGES from "Common/js/Messages";

const DOMAIN = require("../../../secrets.json")["domain"];

class Popup extends Component {
	constructor(props) {
		super(props);

		this.state = {};

		(async () => {
			// TODO better error handling (rewrite get and post) for situations like offline server
			const [res, ok] = await get(`${DOMAIN}/auth/state`);

			if (!ok) {
				this.setState({
					authed: "error",
				});
				return;
			}

			this.setState({
				authed: res.authed,
			});
		})();
	}

	handleSelectWorkspace(workspace) {
		chrome.runtime.sendMessage({
			message: MESSAGES.SELECT_WORKSPACE,
			...workspace,
		});
	}

	render() {
		let bodyComponent;
		switch (this.state.authed) {
			case undefined:
				bodyComponent = (
					<div id="loading">
						<p>Loading...</p>
					</div>
				);
				break;
			case false:
				bodyComponent = <Login />;
				break;
			case true:
				bodyComponent = (
					<WorkspaceSelector onSelectWorkspace={this.handleSelectWorkspace} />
				);
				break;
		}

		return (
			<>
				<h1>Continuum</h1>
				{bodyComponent}
			</>
		);
	}
}

const Login = props => (
	<div id="login">
		<a href={DOMAIN} target="_blank">
			Log in first
		</a>
	</div>
);

export default Popup;
