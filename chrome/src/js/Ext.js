import { call } from "Common/js/call";
import { request } from "graphql-request";

const ENDPOINT = require("../../../secrets.json")["domain"] + "/graphql";

class Ext {
	constructor() {
		this.selectedWorkspace = {
			id: null,
			name: "(None)",
		};
	}

	async createNewBranch({ url, title }) {
		const query = `
mutation addBranchQuery(
	$url: String!,
	$title: String!,
	$workspaceId: ID,
) {
	addBranch(initialStep:{
		url: $url,
		title: $title,
		workspaceId: $workspaceId
	}) {
		id
		steps {
			id
		}
	}
}`;
		const [result, err] = await call(
			request(ENDPOINT, query, {
				url,
				title,
				workspaceId: this.selectedWorkspace.id,
			}),
		);
		if (err) {
			console.log("Error!");
			return;
		}
		console.log(result);

		return result.addBranch.id;
	}

	async createChildBranch(parentBranchId, { id, url, title, openerTabId }) {
		const query = `
mutation addBranchQuery(
	$url: String!,
	$title: String!,
	$parent: ID!,
	$workspaceId: ID,
) {
	addBranch(initialStep:{
		url: $url,
		title: $title,
		parent: $parent,
		workspaceId: $workspaceId
	}) {
		id
		steps {
			id
		}
	}
}`;

		const [result, err] = await call(
			request(ENDPOINT, query, {
				url,
				title,
				parent: parentBranchId,
				workspaceId: this.selectedWorkspace.id,
			}),
		);

		if (err) {
			console.log("Error!");
			console.log(err);
			return;
		}

		console.log(result);

		return result.addBranch.id;
	}

	async addStep(branchId, { id: tabId, url, title, favIconUrl = null }) {
		if (!branchId) {
			console.log("Ignoring step on untracked branch:");
			console.log(tabId, url, title);
			return;
		}

		const query = `
mutation addStepQuery(
	$url: String!,
	$title: String!,
	$branchId: ID!,
	$workspaceId: ID,
	) {
	addStep(step: {
		url: $url,
		title: $title,
		branch: $branchId,
		workspaceId: $workspaceId
	}) {
		id
	}
}`;

		const [result, err] = await call(
			request(ENDPOINT, query, {
				url,
				title,
				branchId,
				workspaceId: this.selectedWorkspace.id,
			}),
		);

		if (err) {
			console.log("Error:");
			console.log(err);
			return;
		}

		console.log(result);
	}

	async selectWorkspace({ id, name }) {
		this.selectedWorkspace = { id, name };
		chrome.storage.local.set({
			workspace: { id, name },
		});

		// load branch tails for workspace
		// remember, the ID of the step doesn't matter, we want the ID of the branch
		const query = `
query workspaceTailQuery($id: ID) {
  viewer {
    workspace(workspaceId: $id) {
      branches {
        tail {
      	  url
      	  timestamp
      	  branch {
      	  	id
      	  }
        }
      }
    }
  }
}
`;

		const [result, err] = await call(request(ENDPOINT, query, { id }));

		if (err) {
			console.log("Error:");
			console.log(err);
			return;
		}

		const {
			viewer: {
				workspace: { branches },
			},
		} = result;

		console.log(branches);

		return branches.map(({ tail: { url, branch: { id: branchId } } }) => ({
			url,
			branchId,
		}));
	}
}

export default Ext;
