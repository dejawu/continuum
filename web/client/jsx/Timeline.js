import React, { Component } from "react";
import { request } from "graphql-request";

import { call } from "Common/js/call";

const ENDPOINT = require("../../../secrets.json")["domain"] + "/graphql";

const TIMELINE_QUERY = `
	query {
		viewer {
			steps {
				id
				title
				url
				favIconUrl
				timestamp
				branch {
					id
				}
				children {
					id
				}
				parent {
					id
				}
				predecessor {
					id
				}
				successor {
					id
					timestamp
				}
			}
		}
	}
`;

class Timeline extends Component {
	constructor(props) {
		super(props);

		this.state = {
			ready: false,
			error: false,
		};

		(async () => {
			const [result, err] = await call(request(ENDPOINT, TIMELINE_QUERY));
			if (err) {
				this.setState({
					error: true,
				});
			}

			console.log(result);

			this.setState({
				steps: result.viewer.steps,
				ready: true,
			});
		})();
	}

	render() {
		if (!this.state.ready) {
			return <p>Loading...</p>;
		}

		if (this.state.error) {
			return <p>Error</p>;
		}

		const stepArr = this.state.steps;

		console.log(stepArr);

		// index steps by step ID
		const stepsById = stepArr.reduce((acc, step) => {
			acc[step.id] = step;
			return acc;
		}, {});

		const branchHeads = stepArr.filter(
			({ predecessor }) => predecessor === null,
		);
		const branchTails = {};

		const branchesAsArrays = branchHeads.reduce((acc, branchHead) => {
			acc[branchHead.id] = stepArr
				.filter(step => {
					return step.branch.id === branchHead.id;
				})
				.sort(({ timestamp: a }, { timestamp: b }) => {
					return a < b;
				});

			branchTails[branchHead.branch.id] =
				acc[branchHead.id][acc[branchHead.id].length - 1];
			return acc;
		}, {});

		let branchLevel = -1;
		const findBranchLevels = branchId => {
			branchLevel += 1;
			// set all steps in this branch to the same level *before* recursing into other branches
			branchesAsArrays[branchId].forEach(step => {
				if (!step.levels) {
					step.levels = [];
				}
				step.nodeLevel = branchLevel;
				step.levels[branchLevel] = "node";
			});

			let step = stepsById[branchTails[branchId].id];
			while (step) {
				step.children.reverse().forEach(({ id: childId }) => {
					findBranchLevels(stepsById[childId].branch.id);
				});

				if (step.predecessor) {
					step = stepsById[step.predecessor.id];
				} else {
					break;
				}
			}
		};

		branchHeads
			.filter(({ parent }) => parent === null)
			.map(({ id }) => id)
			.forEach(branchId => {
				findBranchLevels(branchId);
			});

		const continueBranchBetween = (startTimestamp, endTimestamp, level) => {
			stepArr
				.filter(
					step =>
						step.timestamp > startTimestamp && step.timestamp < endTimestamp,
				)
				.forEach(step => {
					step.levels[level] = "continue";
				});
		};

		// determine at which levels branch forks (L- and T- beams) are needed
		stepArr.forEach(step => {
			step.children.forEach(({ id: childId }) => {
				const child = stepsById[childId];
				step.levels[child.nodeLevel] = "branch";
				continueBranchBetween(step.timestamp, child.timestamp, child.nodeLevel);
			});
		});

		Object.keys(branchesAsArrays).forEach(branchId => {
			branchesAsArrays[branchId].forEach(step => {
				if (step.successor !== null) {
					continueBranchBetween(
						step.timestamp,
						step.successor.timestamp,
						step.nodeLevel,
					);
				}
			});
		});

		// fill empty elements in arrays. I hate this
		stepArr.forEach(step => {
			if (!step.levels.includes("node")) {
				// TODO proper error handling for this situation
				console.log("Step does not contain a node!");
				console.log(step);
				return;
			}

			let i = 0;
			// can't use forEach (or other array functions) because they skip over undefined entries. >:(

			while (step.levels[i] !== "node") {
				if (!step.levels[i]) {
					step.levels[i] = "empty";
				}
				i += 1;
			}

			const hasBranchAfterNode = step.levels.includes("branch");
			for (let i = step.nodeLevel; i < step.levels.length - 1; i += 1) {
				if (!step.levels[i]) {
					if (hasBranchAfterNode) {
						step.levels[i] = "dash";
					} else {
						step.levels[i] = "empty";
					}
				}
			}
		});

		// const flattenedSteps = stepArr.sort((a, b) => (a.timestamp > b.timestamp) ? -1 : 1);
		const flattenedSteps = stepArr.reverse();

		return (
			<div className="timeline">
				<h1>Timeline</h1>
				<div className="timeline-grid">
					{flattenedSteps.map(step => (
						<TimelineStep {...step} key={step.id} />
					))}
				</div>
			</div>
		);
	}
}

class TimelineStep extends Component {
	render() {
		const renderedLevels = this.props.levels.map((nodeType, level) => {
			switch (nodeType) {
				case "branch":
					if (level === this.props.levels.length - 1) {
						return <div className="branch-L" />;
					} else {
						return <div className="branch-T" />;
					}
				case "continue":
					return <div className="branch-I" />;
				case "node":
					return <div className="branch-node" />;
				case "dash":
					return <div className="branch--" />;
				case "empty":
					// TODO if there are no more branches
					return <div className="branch-empty" />;
				default:
					throw new Error("Unhandled nodeType: " + nodeType);
			}
		});

		return (
			<div className="timeline-row" key={this.props.stepId}>
				<div className="timeline-step">
					<p>
						{this.props.parent === null &&
							this.props.predecessor === null &&
							"top-level"}{" "}
						{this.props.title}: {this.props.url}
					</p>
				</div>
				{renderedLevels}
			</div>
		);
	}
}

export default Timeline;
