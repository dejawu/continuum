import "../scss/main.scss";

import React, { Component } from "react";
import { render } from "react-dom";

import Timeline from "./Timeline";

import { get, post } from "Common/js/call";

const CHROME_EXT = Boolean(chrome.runtime);

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {};

		(async () => {
			const [result, ok] = await get("/auth/state");
			if (!ok) {
				console.log("Error:");
				console.log(result);
			} else {
				this.setState({
					authed: result.authed,
				});
			}
		})();
	}

	render() {
		console.log(this.state.authed);
		let bodyComponent;
		switch (this.state.authed) {
			case undefined:
				bodyComponent = (
					<div id="loading">
						<p>Loading...</p>
					</div>
				);
				break;
			case false:
				bodyComponent = (
					<LoginForm
						handleLoginSuccess={() => {
							this.setState({
								authed: true,
							});
						}}
					/>
				);
				break;
			case true:
				bodyComponent = (
					<div id="app">
						<Timeline />
					</div>
				);
				break;
		}

		return bodyComponent;
	}
}

class LoginForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			username: "",
			password: "",
			message: "",
		};
	}

	handleUsernameChange = event => {
		this.setState({ username: event.target.value });
	};

	handlePasswordChange = event => {
		this.setState({ password: event.target.value });
	};

	handleLoginSubmit = async (event = undefined) => {
		if (event) {
			event.preventDefault();
		}

		const [res, ok] = await post("/auth/login", {
			username: this.state.username,
			password: this.state.password,
		});

		if (ok) {
			// this.props.handleLogin(res.username);
			this.props.handleLoginSuccess();
		} else {
			this.setState({ message: res.message });
		}
	};

	handleJoinSubmit = async e => {
		e.preventDefault();
		const [res, ok] = await post("/auth/join", {
			username: this.state.username,
			password: this.state.password,
		});

		if (ok) {
			await this.handleLoginSubmit();
		} else {
			this.setState({ message: res.message });
		}
	};

	render() {
		return (
			<div>
				<form>
					<label>
						Username
						<input
							type="text"
							onChange={this.handleUsernameChange}
							onKeyPress={e => {
								if (e.key === "Enter") {
									this.handleLoginSubmit(e);
								}
							}}
						/>
					</label>

					<label>
						Password
						<input
							type="password"
							onChange={this.handlePasswordChange}
							onKeyPress={e => {
								if (e.key === "Enter") {
									this.handleLoginSubmit(e);
								}
							}}
						/>
					</label>

					<a
						href="#"
						className="button-purple"
						onClick={e => this.handleJoinSubmit(e)}
					>
						Join
					</a>
					<a
						href="#"
						className="button-green"
						onClick={e => this.handleLoginSubmit(e)}
					>
						Log in
					</a>

					{this.state.message !== "" && <p>{this.state.message}</p>}
				</form>
			</div>
		);
	}
}

render(<App />, document.getElementById("root"));
