// app config
const SECRETS = require("../secrets.json");

const bcrypt = require("bcrypt");

// Express and related
const express = require("express");
const app = express();
const path = require("path");
const bodyparser = require("body-parser");

const session = require("express-session");
const mysqlSessionStore = require("express-mysql-session")(session);
const sessionStore = new mysqlSessionStore({
	host: "localhost",
	user: SECRETS.mysql_user,
	password: SECRETS.mysql_pass,
	database: SECRETS.mysql_db,
});

const jsonSuccessOut = (response, payload = {}) => {
	response.status(200).json(payload);
};
const jsonErrorOut = (response, status, message = null, payload = {}) => {
	response.status(status).json({
		...{
			message: message,
		},
		...payload,
	});
};

// Database
const knex = require("knex")({
	client: "mysql2",
	connection: {
		host: "localhost",
		user: SECRETS.mysql_user,
		password: SECRETS.mysql_pass,
		database: SECRETS.mysql_db,
	},
});
const { db, dbq } = require("./server/util");

// GraphQL
const graphqlHTTP = require("express-graphql");
const schema = require("./server/schema/schema");

app.use(bodyparser.json());
app.use(
	session({
		store: sessionStore,
		resave: false,
		saveUninitialized: false,
		secret: (SECRETS.env === "DEV") ? "secret" : require("crypto-random-string")(256)
	}),
);

app.get("/", (request, response) => {
	response.sendFile(path.join(__dirname, "./static/index.html"));
});

app.get("/bundle.js", (req, res) => {
	res.sendFile(path.join(__dirname, "./static/bundle.js"));
});

app.get("/bundle.css", (req, res) => {
	res.sendFile(path.join(__dirname, "./static/bundle.css"));
});

app.post("/auth/join", async (request, response) => {
	if (!request.body.username || !request.body.password) {
		jsonErrorOut(response, 400, "Expected both username and password.");
		return;
	}

	if (request.body.username.length > 20) {
		jsonErrorOut(response, 400, "Username is too long.");
		return;
	}

	if (
		typeof request.body.password !== "string" ||
		request.body.password === ""
	) {
		jsonErrorOut(response, 400, "Password cannot be empty.");
		return;
	}

	const [result, err] = await db(
		knex("users").insert({
			username: request.body.username,
			passhash: await bcrypt.hash(request.body.password, 10),
		}),
	);

	if (err) {
		switch (err.code) {
			case "ER_DUP_ENTRY":
				jsonErrorOut(response, 400, "Username is not available.");
				break;
			default:
				jsonErrorOut(
					response,
					500,
					"Error creating user - please try again later.",
				);
				break;
		}

		return;
	}

	jsonSuccessOut(response);
});

app.post("/auth/login", async (request, response) => {
	if (!request.body.username || !request.body.password) {
		jsonErrorOut(response, 400, "Expected both username and password.");
		return;
	}

	let [dbResult, err] = await db(
		knex
			.select("id", "username", "passhash")
			.where({
				username: request.body.username,
			})
			.from("users"),
	);

	if (err) {
		console.log(err);
		jsonErrorOut(response, 500, "Internal server error.");
		return;
	}

	if (dbResult.length !== 1) {
		jsonErrorOut(response, 401, "Incorrect username or password.");
		return;
	}

	const match = await bcrypt.compare(
		request.body.password,
		dbResult[0].passhash.toString("utf8"),
	);

	if (!match) {
		jsonErrorOut(response, 401, "Incorrect username or password.");
		return;
	}

	request.session.authed = true;
	request.session.userid = dbResult[0].id;
	request.session.username = dbResult[0].username;

	jsonSuccessOut(response, {
		authed: true,
		username: dbResult[0].username,
	});
});

app.post("/auth/logout", (request, response) => {
	request.session.destroy();
	jsonSuccessOut(response);
});

app.get("/auth/state", (request, response) => {
	const authed = Boolean(request.session.authed);
	jsonSuccessOut(response, {
		authed: authed,
		username: authed ? request.session.username : undefined,
	});
});

app.use(
	"/graphql",
	(request, response, next) => {
		if (!request.session.authed) {
			response.status(401).json(
				// mimic graphQL error response format:
				{
					errors: [
						{
							message: "Unauthorized.",
						},
					],
				},
			);
			return;
		}

		// make database connection accessible from GraphQL context
		request.knex = knex;
		request.q = dbq;

		next();
	},
	graphqlHTTP({
		schema: schema,
		graphiql: true,
		formatError: err => {
			console.log(err);
			return err;
		},
	}),
);

app.use(express.static(__dirname + '/static'));

app.listen(3000, () => {
	console.log("HTTP server listening");
});
