const {
	GraphQLObjectType,
	GraphQLID,
	GraphQLString,
	GraphQLList,
	GraphQLNonNull,
} = require("graphql");

const { db } = require("../util");

const { STEP_FIELDS } = require("./STEP_FIELDS");

const User = new GraphQLObjectType({
	name: "User",
	fields: () => {
		const { Step } = require("./Step");
		const { Branch } = require("./Branch");
		const { Workspace } = require("./Workspace");

		return {
			id: {
				type: GraphQLID,
			},
			username: {
				type: GraphQLString,
			},
			steps: {
				type: new GraphQLList(new GraphQLNonNull(Step)),
				async resolve(parent, args, request) {
					const [result, err] = await db(
						request.knex
							.select(...STEP_FIELDS)
							.from("steps")
							.where({
								owner: request.session.userid,
							})
							.orderBy("timestamp", "asc"),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result;
				},
			},
			branches: {
				// all top-level branches
				type: new GraphQLList(new GraphQLNonNull(Branch)),
				async resolve(parent, args, request) {
					const [result, err] = await db(
						request
							.knex("steps")
							.distinct("branch")
							.where({
								owner: request.session.userid,
							}),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result.map(b => ({ id: b.branch }));
				},
			},
			workspaces: {
				type: new GraphQLList(new GraphQLNonNull(Workspace)),
				async resolve(parent, args, request) {
					const [result, err] = await db(
						request
							.knex("workspaces")
							.select("id", "name")
							.where("owner", request.session.userid),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result;
				},
			},
			workspace: {
				type: Workspace,
				async resolve(parent, args, request) {
					if (
						args.workspaceId === null ||
						typeof args.workspaceId === "undefined"
					) {
						return {
							id: null,
							name: "None",
						};
					} else {
						const [result, err] = await db(
							request
								.knex("workspaces")
								.select("id", "name")
								.where({
									owner: request.session.userid,
									id: args.workspaceId,
								}),
						);

						if (err) {
							console.log(err);
							throw new Error("Internal server error.");
						}

						if (result.length !== 1) {
							throw new Error("Invalid workspace.");
						}

						return result[0];
					}
				},
				args: {
					workspaceId: {
						type: GraphQLID,
					},
				},
			},
		};
	},
});

module.exports = { User };
