const {
	GraphQLObjectType,
	GraphQLID,
	GraphQLList,
	GraphQLNonNull,
} = require("graphql");

const { db } = require("../util");

const { STEP_FIELDS } = require("./STEP_FIELDS");

const Branch = new GraphQLObjectType({
	name: "Branch",
	fields: () => {
		const { Step } = require("./Step");

		return {
			id: {
				type: GraphQLID,
			},
			steps: {
				type: new GraphQLList(new GraphQLNonNull(Step)),
				async resolve(parent, args, request) {
					const [result, err] = await db(
						request
							.knex("steps")
							.select(...STEP_FIELDS)
							.where({
								branch: parent.id,
								owner: request.session.userid,
							}),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result;
				},
			},
			head: {
				type: Step,
				async resolve(parent, args, request) {
					const [result, err] = await db(
						request
							.knex("steps")
							.select(...STEP_FIELDS)
							.where({
								branch: parent.id,
								owner: request.session.userid,
							})
							.orderBy("timestamp", "asc")
							.limit(1),
					);

					if (err || result.length !== 1) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result[0];
				},
			},
			tail: {
				type: Step,
				async resolve(parent, args, request) {
					const [result, err] = await db(
						request
							.knex("steps")
							.select(...STEP_FIELDS)
							.where({
								branch: parent.id,
								owner: request.session.userid,
							})
							.orderBy("timestamp", "desc")
							.limit(1),
					);

					if (err || result.length !== 1) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result[0];
				},
			},
		};
	},
});

module.exports = { Branch };
