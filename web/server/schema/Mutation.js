const shortid = require("shortid");
const { GraphQLObjectType, GraphQLNonNull, GraphQLString } = require("graphql");

const { dbq } = require("../util");

const { STEP_FIELDS } = require("./STEP_FIELDS");

const Mutation = new GraphQLObjectType({
	name: "Mutation",
	fields: () => {
		const { Step } = require("./Step");
		const { StepInput } = require("./StepInput");
		const { Branch } = require("./Branch");
		const { Workspace } = require("./Workspace");

		return {
			addBranch: {
				type: Branch,
				async resolve(parent, args, request) {
					const id = shortid.generate();

					let latestId = null;

					// TODO maybe get workspace from parent branch if not top-level?
					if (args.initialStep.parent) {
						let [result, err] = await dbq(
							request
								.knex("steps")
								.select("id")
								.where({
									branch: args.initialStep.parent,
								})
								.orderBy("timestamp", "desc")
								.limit(1),
						);
						if (err) {
							console.log(err);
							throw new Error("Internal server error.");
						}
						if (result.length !== 1) {
							throw new Error("Invalid branch: " + args.initialStep.parent);
						}
						if (result[0].successor) {
							console.log("Latest step already had a successor:");
							console.log(result[0]);
							throw new Error("Internal server error.");
						}

						latestId = result[0].id;
					}

					const initialStep = {
						id: id,
						url: args.initialStep.url,
						title: args.initialStep.title,
						owner: request.session.userid,
						branch: id,
						parent: latestId,
						timestamp: Date.now(),
						workspace: args.initialStep.workspaceId,
					};

					let [result, err] = await dbq(
						request.knex("steps").insert(initialStep),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return {
						id: initialStep.id,
					};
				},
				args: {
					initialStep: {
						type: new GraphQLNonNull(StepInput),
					},
				},
			},
			addStep: {
				type: Step,
				async resolve(parent, args, request) {
					const id = shortid.generate();

					// "branch" field is nullable, but required by this mutation
					if (!args.step.branch) {
						throw new Error("Parameter 'branch' is required.");
					}

					// validate branchId and get predecessor ID
					let [result, err] = await dbq(
						request
							.knex("steps")
							.where({
								branch: args.step.branch,
							})
							.orderBy("timestamp", "desc")
							.limit(1),
					);
					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}
					if (result.length !== 1) {
						throw new Error("Invalid branch: " + args.step.branch);
					}
					if (result[0].successor) {
						console.log("Latest step already had a successor:");
						console.log(result[0]);
						throw new Error("Internal server error.");
					}

					const predecessor = result[0];

					if (predecessor.url === args.step.url) {
						// update existing step
						const updateStep = Object.entries(args.step).reduce(
							(acc, [key, val]) => {
								// map nulls and empty strings to undefined so knex will ignore them
								if (
									val === null ||
									val === "" ||
									// prevent certain fields from being retroactively changed
									[
										"branch",
										"id",
										"predecessor",
										"successor",
										"parent",
										"workspaceId",
									].includes(key)
								) {
									acc[key] = undefined;
									return acc;
								} else {
									acc[key] = val;
									return acc;
								}
							},
							{},
						);

						[result, err] = await dbq(
							request
								.knex("steps")
								.where("id", predecessor.id)
								.update(updateStep),
						);

						if (err) {
							console.log(err);
							throw new Error("Internal server error.");
						}

						// fetch updated predecessor
						// TODO find a way to make this query unnecessary (e.g. compute updated entry locally)
						[result, err] = await dbq(
							request
								.knex("steps")
								.select(...STEP_FIELDS)
								.where("id", predecessor.id),
						);

						return result[0];
					} else {
						// add new step
						const step = {
							id: id,
							url: args.step.url,
							title: args.step.title,
							owner: request.session.userid,
							branch: args.step.branch,
							parent: args.step.parent,
							timestamp: Date.now(),
							predecessor: predecessor.id,
							workspace: args.step.workspaceId,
						};
						[result, err] = await dbq(request.knex("steps").insert(step));
						if (err) {
							console.log(err);
							throw new Error("Internal server error.");
						}

						// update predecessor
						[result, err] = await dbq(
							request
								.knex("steps")
								.where({
									id: predecessor.id,
								})
								.update({
									successor: id,
								}),
						);

						return step;
					}
				},
				args: {
					step: {
						type: new GraphQLNonNull(StepInput),
					},
				},
			},
			addWorkspace: {
				type: Workspace,
				async resolve(parent, args, request) {
					const id = shortid.generate();

					const workspace = {
						id: id,
						name: args.name,
						owner: request.session.userid,
					};

					const [result, err] = await dbq(
						request.knex("workspaces").insert(workspace),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return workspace;
				},
				args: {
					name: {
						type: new GraphQLNonNull(GraphQLString),
					},
				},
			},
		};
	},
});

module.exports = { Mutation };
