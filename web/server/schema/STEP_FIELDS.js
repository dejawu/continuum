module.exports = {
	STEP_FIELDS: [
		"id",
		"url",
		"branch",
		"faviconurl",
		"title",
		"parent",
		"predecessor",
		"successor",
		"timestamp",
		"workspace",
	],
};
