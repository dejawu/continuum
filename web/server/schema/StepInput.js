const {
	GraphQLInputObjectType,
	GraphQLString,
	GraphQLID,
	GraphQLNonNull,
} = require("graphql");

const StepInput = new GraphQLInputObjectType({
	name: "StepInput",
	fields: () => ({
		url: {
			type: new GraphQLNonNull(GraphQLString),
		},
		title: {
			type: new GraphQLNonNull(GraphQLString),
		},
		branch: {
			type: GraphQLID,
		},
		parent: {
			type: GraphQLID,
		},
		favIconUrl: {
			type: GraphQLString,
			defaultValue: "",
		},
		workspaceId: {
			type: GraphQLID,
		},
	}),
});

module.exports = { StepInput };
