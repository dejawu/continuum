const { GraphQLObjectType } = require("graphql");

const { db } = require("../util");

const { User } = require("./User");

const Query = new GraphQLObjectType({
	name: "Query",
	fields: {
		viewer: {
			type: User,
			async resolve(parent, args, request) {
				const [result, err] = await db(
					request
						.knex("users")
						.select("id", "username")
						.where({
							id: request.session.userid,
						}),
				);

				if (err) {
					throw new Error("Internal server error.");
				}

				return result[0];
			},
		},
	},
});

module.exports = { Query };
