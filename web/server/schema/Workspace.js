const {
	GraphQLObjectType,
	GraphQLID,
	GraphQLString,
	GraphQLNonNull,
	GraphQLList,
} = require("graphql");

const { db } = require("../util");

const { STEP_FIELDS } = require("./STEP_FIELDS");

const Workspace = new GraphQLObjectType({
	name: "Workspace",
	fields: () => {
		const { Step } = require("./Step");
		const { Branch } = require("./Branch");

		return {
			id: {
				type: GraphQLID,
			},
			name: {
				type: GraphQLString,
			},
			steps: {
				type: new GraphQLList(new GraphQLNonNull(Step)),
				async resolve(parent, args, request) {
					const [result, err] = await db(
						request
							.knex("steps")
							.select(...STEP_FIELDS)
							.where({
								owner: request.session.userid,
								workspace: parent.id,
							}),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result;
				},
			},
			branches: {
				type: new GraphQLList(new GraphQLNonNull(Branch)),
				async resolve(parent, args, request) {
					const [result, err] = await db(
						request
							.knex("steps")
							.distinct("branch")
							.where({
								owner: request.session.userid,
								workspace: parent.id,
							}),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result.map(b => ({ id: b.branch }));
				},
			},
		};
	},
});

module.exports = { Workspace };
