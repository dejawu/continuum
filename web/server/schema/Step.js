const {
	GraphQLObjectType,
	GraphQLID,
	GraphQLString,
	GraphQLFloat,
	GraphQLNonNull,
	GraphQLList,
} = require("graphql");

const { db } = require("../util");

const { STEP_FIELDS } = require("./STEP_FIELDS");

const Step = new GraphQLObjectType({
	name: "Step",
	fields: () => {
		const { Branch } = require("./Branch");
		const { Workspace } = require("./Workspace");

		return {
			id: {
				type: new GraphQLNonNull(GraphQLID),
			},
			url: {
				type: new GraphQLNonNull(GraphQLString),
			},
			title: {
				type: GraphQLString,
			},
			branch: {
				type: Branch,
				async resolve(parent, args, request) {
					return {
						id: parent.branch,
					};
				},
			},
			children: {
				type: new GraphQLList(new GraphQLNonNull(Branch)),
				async resolve(parent, args, request) {
					const [result, err] = await db(
						request
							.knex("steps")
							.select("id")
							.where({
								parent: parent.id,
								owner: request.session.userid,
							})
							.orderBy("timestamp", "desc"),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result;
				},
			},
			parent: {
				type: Step,
				async resolve(parent, args, request) {
					if (parent.parent === null) {
						return null;
					}

					const [result, err] = await db(
						request
							.knex("steps")
							.select(...STEP_FIELDS)
							.where({
								id: parent.parent,
								owner: request.session.userid,
							}),
					);

					if (result.length !== 1) {
						console.log(
							"Expected exactly one result for parent but got " + result.length,
						);
						throw new Error("Internal server error.");
					}

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result[0];
				},
			},
			predecessor: {
				type: Step,
				async resolve(parent, args, request) {
					if (parent.predecessor === null) {
						return null;
					}

					const [result, err] = await db(
						request
							.knex("steps")
							.select(...STEP_FIELDS)
							.where({
								id: parent.predecessor,
								owner: request.session.userid,
							}),
					);

					if (result.length !== 1) {
						console.log(
							"Expected exactly one result for predecessor but got " +
								result.length,
						);
						throw new Error("Internal server error.");
					}

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result[0];
				},
			},
			successor: {
				type: Step,
				async resolve(parent, args, request) {
					if (parent.successor === null) {
						return null;
					}

					const [result, err] = await db(
						request
							.knex("steps")
							.select(...STEP_FIELDS)
							.where({
								id: parent.successor,
								owner: request.session.userid,
							}),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					if (result.length !== 1) {
						console.log(
							"Expected exactly one result for successor but got " +
								result.length,
						);
						throw new Error("Internal server error.");
					}

					return result[0];
				},
			},
			timestamp: {
				type: new GraphQLNonNull(GraphQLFloat),
			},
			favIconUrl: {
				type: GraphQLString,
			},
			workspace: {
				type: Workspace,
				async resolve(parent, args, request) {
					const [result, err] = await db(
						request
							.knex("workspaces")
							.select("id", "name")
							.where({
								owner: parent.id,
							}),
					);

					if (err) {
						console.log(err);
						throw new Error("Internal server error.");
					}

					return result[0];
				},
			},
		};
	},
});

module.exports = { Step };
