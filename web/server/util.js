const PQueue = require("p-queue");

const pq = new PQueue({ concurrency: 1 });

module.exports = {
	// TODO replace all usages of this with "call"
	// wrapper function to make queries Go-like
	db: async query => {
		try {
			const result = await query;
			return [result, null];
		} catch (err) {
			return [null, err];
		}
	},
	// queue for operations that need to be done synchronously
	// TODO block operations by branch ID instead of blocking all
	dbq: async query => {
		try {
			const result = await pq.add(() => query);
			return [result, null];
		} catch (err) {
			return [null, err];
		}
	},
};
