# Continuum

**No more of this:**

![](https://i.imgur.com/tN1fPqP.png)

# Overview

Continuum is my latest attempt at a software solution to my tab hoarding compulsion. After several prototypes failed to encompass the use cases that my ravenous browsing demands, I realized the following are all different parts of the same problem:

- Bookmarks
	- The comments left on said bookmarks
- "Read later" list
- Currently open tabs
- Web history

Going beyond existing solutions like OneTab, Pocket/Pinboard, and Tree Style tabs, Continuum aims to use a combination of features to acknowledge that these are all stages in the single continuous process of bringing external content into your expanded working knowledge. Many of these solutions have been implemented independently before but never integrated.

# Features

## Tree-style history

When I browse, for example if I'm in a deep Wikipedia trawl, the tabs I open aren't just meaningful on their own, they also fit into the context of how I got there and what other tabs I opened from that one.

Rather than storing history linearly like current browsers, Continuum stores history as a tree, retaining both the relationship between opened tabs as well as the original chronological browsing order.

![](https://i.imgur.com/goszt77.png)

(Prototype tree-style history shown above. Each black square, which will eventually be replaced with a favicon, represents a browsing step.)

## Workspaces

Generally when browsing I am focused on one task, which might itself require many tabs. This is a problem when tabs relevant to my current task bleed over or mix with the other open tabs I've stockpiled. This can be delineated with windows, but I might have multiple windows open when working on the same task, and the irrelevant tabs shouldn't even be there anyway. Without an extension like OneTab, Chrome doesn't even provide a way for you to put these irrelevant tabs away (besides closing the window and later restoring it with history, which can be unreliable).

Workspaces will silo your tabs *and their history* so you can focus on the task at hand and bring back your other tabs when you're done.

Workspaces are not a new concept; see Refresh in the "Existing demand" section below.

## Sync

Most of my unclosed tabs are on my phone. (When I switched phones I exported 800 tabs from my old one!) This is frustrating because I have no way of bringing them onto a desktop browser and marking them read from there. (Chrome tab sync does not allow you to close tabs on other devices.)

Continuum provides a single source of truth for your workspaces, which are accessible across any browser (as long as the Continuum extension is available). When you switch devices, you re-open the workspace and continue right where you were.

## History-linked notes

The main reason I am hesitant to close a tab is the fear that once I do, the useful/relevant/interesting information in the page will be lost to the aether, and half a year later I'll remember it and want to bring it up again and not be able to find it.

In addition to saving all browsing history and making it accessible, Continuum will allow you to pin a comment or note to a step in history. This differs from existing bookmarking services, which do not include the browsing context with a bookmark and its notes.

I find that writing helps me to internalize and process the knowledge in a page as well, improving my retention of its contents. The plan is to also make notes searchable eventually.

## Other features

Some of these are just ideas or planned for later on.

- Full history search
	- Chrome's history search is incredibly weak and rarely pulls in any history from more than a few months ago. This is a good opportunity to rebuild it properly.
- Page indexing
	- Going a step further, Continuum could even pull the contents of browsed pages and archive and index them to make them searchable. If a page is browsed multiple times or by multiple people, it could even create an archival timeline.
- Flagged or pinned tabs; tags
	- Mark a tab for later reading, or tag it to label it with a category. Later, see a list of and be able to jump to all history steps with a tag or flag.

# Existing demand and inspiration

Many others, particularly in Hacker News, have expressed the need for a solution to this problem:

- [Refresh](https://refresh.study/): A design case study that served as a major inspiration for Continuum.
	- [HN Thread](https://news.ycombinator.com/item?id=17638477)
- [Open Tabs are Cognitive Spaces](https://rybakov.com/blog/open_tabs_are_cognitive_spaces/): An essay from a fellow tab hoarder articulating why tabs stay open.
	- [HN Thread](https://news.ycombinator.com/item?id=16671957)
- [Firefox Experiments](https://news.ycombinator.com/item?id=19305389): A comment from someone looking for a similar tab solution. (I regret not commenting on this.)
- [Tabbed browsing: a lousy band-aid over poor browser document and state management](https://old.reddit.com/r/dredmorbius/comments/256lxu/tabbed_browsing_a_lousy_bandaid_over_poor_browser/)
- [Firefox and Ridiculous Numbers of Tabs](https://news.ycombinator.com/item?id=14823807): The article doesn't have much to do with this issue, but the comments turned into a huge discussion about tab management.

# Development

I can use all the help I can get! If Continuum presents a compelling solution to you and you would like to contribute, please reach out by emailing me at kevin@kevinywu.com. Anybody working anywhere on the stack, particularly designers and frontend devs, would be helpful.

## Building

Run `npm install`, then `npm run watch` for frontend build and `npm start` to start the server.
